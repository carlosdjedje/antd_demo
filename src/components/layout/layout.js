import React from 'react';
// import 'antd/dist/antd.css';
import { Layout, Menu, Row, Col, Dropdown, Avatar, Badge } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    DashboardOutlined,
    ShoppingCartOutlined,
    BellOutlined
} from '@ant-design/icons';


import './layout.css';

const { Header, Sider, Content, Footer } = Layout;

const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
                Perfil
        </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
                Sair
        </a>
        </Menu.Item>
    </Menu>
);


class MainLayout extends React.Component {

    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <Layout style={{ minHeight: '100vh' }}>
                <Sider theme="dark" className="antd-layout-sider">
                    <div className="antd-layout-brand">
                        <div className="antd-layout-logo">
                            <img alt="logo" src="./antd_logo.svg" />
                            <h1>AntD Admin</h1>
                        </div>
                    </div>
                    <div className="antd-side-menu-container">
                        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                            <Menu.Item key="1" icon={<DashboardOutlined />}>
                                Dashboard
                            </Menu.Item>
                            <Menu.Item key="2" icon={<UserOutlined />}>
                                Users
                            </Menu.Item>
                            <Menu.Item key="3" icon={<ShoppingCartOutlined />}>
                                Posts
                            </Menu.Item>
                        </Menu>
                    </div>
                </Sider>
                <Layout className="site-layout">
                    <Header className="antd-layout-header">
                        {/* {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: this.toggle,
                        })} */}
                        <Row>
                            <Col span={6}>
                                <div>
                                    <h3>
                                        Microsoft
                                    </h3>
                                </div>
                            </Col>
                            <Col span={18}>
                                <Row justify="end">
                                    <Col span={2}>
                                        <Badge dot>
                                            <BellOutlined style={{ fontSize: '23px', color:'#c1c1c1' }} />
                                        </Badge>
                                    </Col>
                                    <Col>
                                        <Dropdown overlay={menu}>
                                            <a href="www.emo.com" className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                                <Avatar icon={<UserOutlined />} />
                                            </a>
                                        </Dropdown>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            minHeight: 280,
                        }}
                    >
                        {this.props.children}
                    </Content>
                </Layout>
            </Layout>
        );
    }
}
export default MainLayout;