import React from 'react';
// import 'antd/dist/antd.css';
import { Statistic, Card, Row, Col, Table, Button, Tag, Breadcrumb } from 'antd';
import { ArrowUpOutlined, ArrowDownOutlined, DashboardOutlined } from '@ant-design/icons';



class Dashboard extends React.Component {

    render() {

        const dataSource = [
            {
                key: '1',
                data: '02-02-2020 00:00:00',
                estado: ['SUCESSO']
            },
            {
                key: '2',
                data: '02-02-2020 00:00:00',
                estado: ['ERRO']
            }, {
                key: '1',
                data: '02-02-2020 00:00:00',
                estado: ['SUCESSO']
            },
            {
                key: '2',
                data: '02-02-2020 00:00:00',
                estado: ['ERRO']
            }, {
                key: '1',
                data: '02-02-2020 00:00:00',
                estado: ['SUCESSO']
            }
        ];

        const columns = [
            {
                title: 'Data',
                dataIndex: 'data',
                key: 'data',
            },
            {
                title: 'Estado',
                dataIndex: 'estado',
                key: 'estado',
                render: tags => (
                    <span>
                        {tags.map(tag => {
                            let color = tag.length > 5 ? 'green' : 'green';
                            if (tag === 'ERRO') {
                                color = 'volcano';
                            }
                            // if (tag === 'SUCESSO') {
                            //     color = 'volcano';
                            // }
                            return (
                                <Tag color={color} key={tag}>
                                    {tag.toUpperCase()}
                                </Tag>
                            );
                        })}
                    </span>
                )
            },
            {
                title: 'Actions',
                dataIndex: 'actions',
                key: 'actions',
                render: (text, record) => {
                    return (
                        <Button>
                            Detalhes
                        </Button>
                    )
                }
            }
        ];


        return (
            <div>
                <div>
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <DashboardOutlined />
                            <span>Dashboard</span>
                        </Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <Row style={{ marginTop: '25px' }} gutter={16}>
                    <Col span={6}>
                        <Card>
                            <Statistic
                                title="Sucesso"
                                value={11}
                                precision={2}
                                valueStyle={{ color: '#3f8600' }}
                                prefix={<ArrowUpOutlined />}

                            />
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card>
                            <Statistic
                                title="Erro"
                                value={9}
                                precision={2}
                                valueStyle={{ color: '#cf1322' }}
                                prefix={<ArrowDownOutlined />}

                            />
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card>
                            <Statistic
                                title="Sucesso"
                                value={11}
                                precision={2}
                                valueStyle={{ color: '#3f8600' }}
                                prefix={<ArrowUpOutlined />}

                            />
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card>
                            <Statistic
                                title="Erro"
                                value={9}
                                precision={2}
                                valueStyle={{ color: '#cf1322' }}
                                prefix={<ArrowDownOutlined />}
                            />
                        </Card>
                    </Col>
                </Row>
                <Row style={{ marginTop: '25px' }}>
                    <Col span={24}>
                        <Card>
                            <Table dataSource={dataSource} columns={columns} />
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default Dashboard;