import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';


import MainLayout from './components/layout/layout';
import Dashboard from './components/dashboard/dashboard';

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <MainLayout>
            <Route exact path="/" component={Dashboard} />
            <Route exact path="/dashboard" component={Dashboard} />
          </MainLayout>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
